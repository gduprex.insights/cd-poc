CD PoC

Areas to consider:
------------------

 1. Build only what has changed
 
When we have multiple services we ideally would only build those that have changed. This includes their dependencies. If 
we do this then we need to perform a partial update of QA, Staging & Production. This requires not modifying any other 
service versions. To do this we need to understand which versions will change & which stay at their current versions. This
also includes infrastructure.
 
 2. Support reusable jobs in pipeline

This is fairly well described by existing literature, in particular the Gitlab EE pipeline build itself. 

 3. Merge request jobs
 
I don't imagine that any job other than ./gradlew build 

 4. Deployment speed
 
Currently creating a new ASG with a single instance & destroying the old one takes 2:30 - 3mins. Not sure what can be 
done to speed this up. Instance creation takes around 20/30 secs.

 5. Shared libraries
 
This problem is completely solved by the `buildDependents` task which is described here: 
https://docs.gradle.org/current/userguide/multi_project_builds.html#sec:multiproject_build_and_test

The rules become:

- Any project defined in `libs` should built with `./gradlew buildDependents` as projects produce binaries for 
others to depend on 
- Any project defined in `svcs` or `tools` should built with `./gradlew build` as projects here produce a standalone binary
 
 6. Artifact cache

Enabling the Gradle local build cache is straightforward (`org.gradle.caching=true`). As all runners share the same 
Docker mount point they automatically gain the benefit of previous builds from a local build cache. First time local 
development will need to build everything but we have powerful laptops so that shouldn't be a problem. If we have 
multiple EC2 instances with Gitlab Runners then maybe a remote build cache would help.

 7. Rebuild production in another AWS account

To rebuild production in another account we need to know the versions of all services, tools & infrastructure (if 
multiple TF state files are at play)

 8. Parallel tests

First, if we enable `org.gradle.parallel=true` then we get parallel testing for free. If that is insufficient Gradle has 
the test setting `maxParallelForks` (https://docs.gradle.org/current/userguide/java_testing.html#sec:test_execution) to 
support JVM level test parallelisation. I would expect this to be sufficient for some time. Gitlab Runner EC2 boxes 
would need updated to 