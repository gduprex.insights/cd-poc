import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

import com.codeborne.selenide.Condition;
import org.junit.Test;
import org.openqa.selenium.By;

public class HelloTest {

    private final String url = "http://" + System.getProperty("HOST", "localhost:8080");

    @Test
    public void userCanLoginByUsername() {
        System.err.println("Connecting to: " + url);
        open(url);
        $(By.id("msg")).shouldHave(Condition.text("Helloworld!"));
    }
}