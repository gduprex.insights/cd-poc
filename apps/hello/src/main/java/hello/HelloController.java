package hello;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    private static final String MAIN_HTML_TEMPLATE = "/main.html.template";
    private static final String VERSION_TOKEN = "%VERSION%";
    private static final String MSG_TOKEN = "%MSG%";

    private final MessageRepository repository;

    public HelloController(MessageRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/")
    public String hello() {
        String message = repository.findById(1L).map(Message::getContent).orElse("<Not Found>");
        return createResponse(message);
    }

    private static String createResponse(String message) {
        try {
            StringBuilder builder = new StringBuilder();
            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(HelloController.class.getResource(MAIN_HTML_TEMPLATE).openStream(),
                            StandardCharsets.UTF_8))) {
                String s;
                while ((s = in.readLine()) != null) {
                    builder.append(s);
                }
            }
            return builder.toString()
                    .replace(VERSION_TOKEN, getVersion())
                    .replace(MSG_TOKEN, message);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String getVersion() {
        String version =  HelloController.class.getPackage().getImplementationVersion();
        return (version == null ? "DEV" : version);
    }
}
