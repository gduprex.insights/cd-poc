create table message (
    id SERIAL PRIMARY KEY,
    content text not null
);
insert into message (id, content) values (1, 'Helloworld!');