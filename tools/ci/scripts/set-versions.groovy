import com.amazonaws.auth.InstanceProfileCredentialsProvider
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import groovy.json.JsonSlurper

@Grab('com.amazonaws:aws-java-sdk-s3:1.11.602')

// ---------------------------------------------------------------------------------------------------------------------
// Stores deployed service versions in S3 using the following scheme:
//     KEY(current/<service_name>) = VALUE(<service_version>)
// ---------------------------------------------------------------------------------------------------------------------

String region = System.getProperty('REGION', 'us-east-1')
String bucket = System.getProperty('BUCKET_NAME', '23457890-76545676776589876')
String input = System.getProperty('INPUT', 'versions.tfvars.json')

// Connect to S3
AmazonS3 s3 = AmazonS3ClientBuilder.standard()
        .withRegion(region)
        .withCredentials(InstanceProfileCredentialsProvider.getInstance())
//        .withCredentials(new ProfileCredentialsProvider())
        .build();

// Update all current versions
new JsonSlurper().parse(new File(input)).each { String key, String value ->
    def artifact = key.substring(0, key.indexOf('_version'))
    s3.putObject(bucket, "current/${artifact}", value)
}
