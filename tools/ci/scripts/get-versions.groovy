import com.amazonaws.auth.InstanceProfileCredentialsProvider
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import groovy.json.JsonOutput

@Grab('com.amazonaws:aws-java-sdk-s3:1.11.602')

// ---------------------------------------------------------------------------------------------------------------------
// Calculates service versions to deploy
// ---------------------------------------------------------------------------------------------------------------------


// TODO: Consider parameterising this
List<String> artifacts = ['app']

String region = System.getProperty('REGION', 'us-east-1')
String bucket = System.getProperty('BUCKET_NAME', '23457890-76545676776589876')
String commit = System.getProperty('COMMIT', '0.0.1-SNAPSHOT')
String output = System.getProperty('OUTPUT', 'versions.tfvars.json')

// Connect to S3
AmazonS3 s3 = AmazonS3ClientBuilder.standard()
        .withRegion(region)
        .withCredentials(InstanceProfileCredentialsProvider.getInstance())
//        .withCredentials(new ProfileCredentialsProvider())
        .build();

// Calculate version to deploy
Map<String, String> versions = [:]
artifacts.each { artifact ->
    def key = artifact + '_version'

    // Get current version
    def currentKey = "current/${artifact}"
    if (s3.doesObjectExist(bucket, currentKey)) {
        def curr = s3.getObject(bucket, currentKey).getObjectContent().getText('UTF-8')
        versions.put(key, curr)
    }

    // Check for new version with this commit
    if (s3.doesObjectExist(bucket, "commits/${commit}/${artifact}-${commit}.jar")) {
        versions.put(key, commit)
    }
}

// Output
new File(output).withWriter { writer -> writer.writeLine(JsonOutput.toJson(versions)) }