# CD PoC

A continuous delivery PoC using Gitlab CD features & AWS

### TODO

 - Configure VPC peering connection from BUILD <-> QA
 - Add production infrastructure
 - Determine minimum IAM role for Gitlab Runner instance
 - Add MR pipeline support
 - Investigate review apps support for QA instances
 - Add a Gitlab runner for "serial" jobs (e.g. deployment) (See: https://forum.gitlab.com/t/how-to-block-pipeline-steps-from-running-in-parallel-across-different-pipelines/20605)
 - Investigate how to perform blue/green switch on production
 - Calculate whether Terraform plan will change infra. See: 
    * https://www.terraform.io/docs/commands/plan.html 
    * https://learn.hashicorp.com/terraform/development/running-terraform-in-automation
 - Breakup main pipeline file (See: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab-ci.yml)
 
### DONE

 - Rewrite Gradle configuration as multi-project build
 - Add RDS instance to hello service
 - Add artifact S3 bucket to build env