variable "region" {}
variable "token" {}
variable "instance_type" {
  default = "t2.micro"
}
variable "ami" {
  default = "ami-0b898040803850657" // Amazon Linux 2 AMI 2.0.20190618 x86_64 HVM gp2
}