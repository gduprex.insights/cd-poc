// https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html
provider "aws" {
  profile = "default"
  region  = var.region
  version = "~> 2.16.0"
}

provider "template" {
  version = "~> 2.1.0"
}

# ----------------------------------------------------------------------------------------
# Store state in Terraform Cloud

terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "cd-poc"

    workspaces {
      name = "build"
    }
  }
}

# ----------------------------------------------------------------------------------------
# NETWORKING
resource "aws_vpc" "build" {
  cidr_block = "10.0.1.0/24"
  tags = {
    Name = "build"
  }
}

resource "aws_subnet" "subnet" {
  cidr_block = "10.0.1.0/25"
  vpc_id     = aws_vpc.build.id
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.build.id
}

resource "aws_route" "route" {
  route_table_id         = aws_vpc.build.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}

# ----------------------------------------------------------------------------------------

resource "aws_security_group" "runner" {
  name   = "runner-security-group"
  vpc_id = aws_vpc.build.id

  # Allow all inbound SSH traffic from me
  //  ingress {
  //    from_port   = 22
  //    to_port     = 22
  //    protocol    = "tcp"
  //    cidr_blocks = ["212.36.33.254/32"]
  //  }

  # Allow all HTTPS outbound (required for provisioning)
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all HTTP outbound (required for provisioning)
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow outbound Postgres traffic (required for provisioning)
  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    security_groups = ["sg-0ae79c1b03a2818c8"]
  }

  tags = {
    Name = "gitlab-runners"
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer"
  public_key = file("${path.module}/../key.pub")
}

# ----------------------------------------------------------------------------------------------------------------------
# SECURITY

resource "aws_iam_role" "runner-role" {
  name               = "ServiceRoleForGitlabRunner"
  description        = "Allows Gitlab Runner to call AWS services required as part of the build pipeline"
  assume_role_policy = data.aws_iam_policy_document.instance-assume-role-policy.json
}

resource "aws_iam_policy_attachment" "attach-full-policy" {
  name       = "attach-full-access"
  roles      = [aws_iam_role.runner-role.name]
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess" # TODO: Ideally this would be narrower!
}

resource "aws_iam_instance_profile" "runner-instance-profile" {
  role = aws_iam_role.runner-role.name
}

# ----------------------------------------------------------------------------------------------------------------------

resource "aws_instance" "runner" {
  count                       = 1
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.deployer.id
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.subnet.id
  iam_instance_profile        = aws_iam_instance_profile.runner-instance-profile.name
  vpc_security_group_ids      = [aws_security_group.runner.id]
  user_data                   = data.template_file.runner-provision.rendered
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    Name = "gitlab-runner-${count.index}"
  }

  # TODO: Unregister runner when destroy
  //  provisioner "remote-exec" {
  //    when    = "destroy"
  //    command = ""
  //  }
}

# ----------------------------------------------------------------------------------------------------------------------

resource "aws_s3_bucket" "artifacts" {
  bucket_prefix = "cd-poc"
  acl           = "private"
  versioning {
    enabled = false
  }

  lifecycle_rule {
    enabled = true
    expiration {
      days = 30
    }
  }
}

resource "aws_s3_bucket_public_access_block" "artifacts_block" {
  bucket = aws_s3_bucket.artifacts.id

  # Block all public access
  block_public_acls       = true
  ignore_public_acls      = true
  restrict_public_buckets = true
  block_public_policy     = true
}

