data "template_file" "runner-provision" {
  template = file("${path.module}/provision.sh")

  vars = {
    region = var.region
    token = var.token
  }
}

// Allow EC2 instances to assume a role
data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    effect = "Allow"
    actions = [ "sts:AssumeRole" ]
    principals {
      identifiers = [ "ec2.amazonaws.com"]
      type = "Service"
    }
  }
}