#!/bin/bash

# Install & start docker
yum install docker -y
service docker start

# Install Gitlab runner
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
yum install gitlab-runner -y
mkdir -p /cache/gradle
mkdir -p /cache/groovy

# Register runner
gitlab-runner register \
    --registration-token "${token}" \
    --url https://gitlab.com/ \
    --executor docker \
    --description "ec2@`hostname -I`" \
    --docker-image "alpine:latest" \
    --docker-volumes "/cache/gradle:/cache/gradle:rw" \
    --docker-volumes "/cache/groovy:/cache/groovy:rw" \
    -n

# Start
gitlab-runner start