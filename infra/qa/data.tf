data "template_file" "provision" {
  template = file("${path.module}/provision.sh")

  vars = {
    s3_bucket_name = var.s3_bucket_name
    app_version = var.app_version
    app_name = var.app_name
    region = var.region
    db_secret_name = aws_secretsmanager_secret.svc.name
    db_hostname = aws_db_instance.db.address
    db_port = aws_db_instance.db.port
    db_name = aws_db_instance.db.name
  }
}

// Allow EC2 instances to assume a role
data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    effect = "Allow"
    actions = [ "sts:AssumeRole" ]
    principals {
      identifiers = [ "ec2.amazonaws.com"]
      type = "Service"
    }
  }
}

// Allow EC2 instances to read RDS password for service
data "aws_iam_policy_document" "instance-read-RDS-password" {
  statement {
    actions = ["secretsmanager:GetSecretValue"]
    resources = [aws_secretsmanager_secret.svc.arn]
    effect = "Allow"
  }
}