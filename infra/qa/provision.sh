#!/bin/bash
set -e

# Install Java
amazon-linux-extras install java-openjdk11 -y

# Install jq
yum install jq -y

# Get app from S3 & unzip
mkdir /opt/hello
cd /opt/hello
aws s3 cp s3://${s3_bucket_name}/commits/${app_version}/${app_name}-${app_version}.jar hello.jar

# Add system user
useradd -r hello

# Configure ownership
chown -R hello:hello /opt/hello/
chmod 500 /opt/hello/*.jar

# Create service, auto-run & start
cat << EOF > /etc/init.d/hello
#!/bin/bash

# Read DB info
SECRET=\$(aws secretsmanager get-secret-value --secret-id ${db_secret_name} --region ${region} | jq -r ".SecretString" | jq . -M)

# Start service
java -jar /opt/hello/hello.jar \
    --spring.datasource.host=${db_hostname} \
    --spring.datasource.port=${db_port} \
    --spring.datasource.database=${db_name} \
    --spring.datasource.username=\$(echo \$SECRET | jq -r .username) \
    --spring.datasource.password=\$(echo \$SECRET | jq -r .password)
EOF
chmod +x /etc/init.d/hello
# TODO: Ensure service is automatically started
service hello start