variable "s3_bucket_name" {
  default = "cd-poc-0987654567898765421" // TODO: Probably shouldn't have a default
}
variable "app_version" {
  default = "a3619fdf" // First build used when no "current" version set
}
variable "app_name" {
  default = "app"
}
variable "region" {
  default = "us-east-1"
}
variable "ami" {
  default = "ami-0b898040803850657" // Amazon Linux 2 AMI 2.0.20190618 x86_64 HVM gp2
}
variable "db_password" {}