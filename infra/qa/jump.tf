////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Jump Server Configuration - gives access to QA VPC
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

resource "aws_security_group" "jump" {
  name   = "jump-security-group"
  vpc_id = aws_vpc.poc.id

  # Allow all inbound SSH traffic from me
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["212.36.33.254/32"]
  }

  # Allow all outbound into the VPC
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.poc.cidr_block]
  }
}

resource "aws_key_pair" "jump" {
  key_name   = "jump-key"
  public_key = file("${path.module}/../key.pub")
}

resource "aws_instance" "jump" {
  count                       = 1
  ami                         = var.ami
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.jump.id
  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.app-instance-profile.name
  subnet_id                   = aws_subnet.sub1.id
  vpc_security_group_ids      = [aws_security_group.jump.id]
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    Name = "jump"
  }
}