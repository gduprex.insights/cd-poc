// https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html
provider "aws" {
  profile = "default"
  region  = var.region
  version = "~> 2.16.0"
}

provider "template" {
  version = "~> 2.1.0"
}

provider "postgresql" {
  version = "~> 1.1.0"
  host = "10.0.2.108" # TODO: Temp! - revert once DB is not publically accessible
  superuser = false
  username = aws_db_instance.db.username
  password = aws_db_instance.db.password
  database = aws_db_instance.db.name
}

provider "random" {
  version = "2.1.2"
}

# ----------------------------------------------------------------------------------------
# Store state in Terraform Cloud

terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "cd-poc"

    workspaces {
      name = "qa"
    }
  }
}

resource "aws_vpc" "poc" {
  cidr_block = "10.0.2.0/24"
  tags = {
    Name = "qa"
  }
}

# ----------------------------------------------------------------------------------------
# 2x subnets (required for ALB)

resource "aws_subnet" "sub1" {
  cidr_block = "10.0.2.0/25"
  vpc_id     = aws_vpc.poc.id
}

resource "aws_subnet" "sub2" {
  cidr_block = "10.0.2.128/25"
  vpc_id     = aws_vpc.poc.id
}

# ----------------------------------------------------------------------------------------
# Internet gateway (required to route traffic into the VPC)

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.poc.id
}

# ----------------------------------------------------------------------------------------
# Public route (directs _all_ internet gateway traffic to VPC)

resource "aws_route" "route" {
  route_table_id         = aws_vpc.poc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gw.id
}

# ----------------------------------------------------------------------------------------
# ALB Security group (all only TCP (port 80) traffic in, all traffic out)

resource "aws_security_group" "alb" {
  name   = "alb-security-group"
  vpc_id = aws_vpc.poc.id

  # Allow all inbound TCP traffic
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "apps" {
  name   = "apps-security-group"
  vpc_id = aws_vpc.poc.id

  # Allow all TCP traffic only from ALB
  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    security_groups = [aws_security_group.alb.id]
  }

  # Allow all inbound traffic from jump server
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    security_groups = [aws_security_group.jump.id]
  }

  # Allow all HTTPS outbound (required for S3 bucket reads)
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow all HTTP outbound (required EC2 instance user data config)
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow Postgres traffic to VPC
  egress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.poc.cidr_block]
  }
}

# ----------------------------------------------------------------------------------------
# ALB (internet facing )

resource "aws_alb" "lb" {
  name               = "alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb.id]
  subnets = [
    aws_subnet.sub1.id,
    aws_subnet.sub2.id
  ]
  depends_on = [aws_internet_gateway.gw]
}

resource "aws_alb_listener" "http-listener" {
  load_balancer_arn = aws_alb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Service unavailable"
      status_code  = "503"
    }
  }
}

resource "aws_alb_listener_rule" "routing" {
  listener_arn = aws_alb_listener.http-listener.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.group.arn
  }

  condition {
    field  = "host-header"
    values = ["*.*"]
  }
}

resource "aws_alb_target_group" "group" {
  protocol             = "HTTP"
  name_prefix          = "apps-"
  port                 = 8080
  vpc_id               = aws_vpc.poc.id
  target_type          = "instance"
  deregistration_delay = 1 # TODO: With Spring Boot services this will need to be longer
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    interval            = 5
    timeout             = 3
  }
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    Name = "apps-target-group"
  }
}

# ----------------------------------------------------------------------------------------------------------------------
# Build IAM role & configure which entities can assume the role
resource "aws_iam_role" "role" {
  name               = "ServiceRoleForEC2AppInstances"
  assume_role_policy = data.aws_iam_policy_document.instance-assume-role-policy.json
}

# Attach S3 read-only policy to the role
resource "aws_iam_policy_attachment" "attach-policy" {
  name       = "test-attachment"
  roles      = [aws_iam_role.role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
}

resource "aws_iam_policy" "read-rds-password" {
  policy = data.aws_iam_policy_document.instance-read-RDS-password.json
}

# Attach S3 read-only policy to the role
resource "aws_iam_policy_attachment" "attach-rds-secret-policy" {
  name       = "read-RDS-password-attachment"
  roles      = [aws_iam_role.role.name]
  policy_arn = aws_iam_policy.read-rds-password.arn
}

# Build an instance profile resource which allows EC2 to assume role
resource "aws_iam_instance_profile" "app-instance-profile" {
  role = aws_iam_role.role.name
}
# ----------------------------------------------------------------------------------------------------------------------

resource "aws_launch_configuration" "apps-launch-configuration" {
  name_prefix                 = "v${var.app_version}-"
  image_id                    = var.ami
  instance_type               = "t2.micro"
  iam_instance_profile        = aws_iam_instance_profile.app-instance-profile.name
  key_name                    = aws_key_pair.jump.id
  user_data                   = data.template_file.provision.rendered
  associate_public_ip_address = true
  security_groups             = [aws_security_group.apps.id]

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_autoscaling_group" "apps-autoscaling-group" {
  name_prefix                 = "v${var.app_version}-"
  launch_configuration = aws_launch_configuration.apps-launch-configuration.name
  vpc_zone_identifier  = [aws_subnet.sub1.id]
  health_check_grace_period = 15 # TODO: Is this sufficient?
  min_size         = 1
  max_size         = 2
  min_elb_capacity = 1

  target_group_arns = [aws_alb_target_group.group.arn]

  # Ensure secrets are available as is schema
  depends_on = [
    postgresql_schema.schema,
    aws_secretsmanager_secret_version.db-creds
  ]

  tags = [
    {
      key                 = "Name"
      value               = "app-${var.app_version}"
      propagate_at_launch = true
    }
  ]

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "db" {
  name   = "db-security-group"
  vpc_id = aws_vpc.poc.id

  # Only postgres in from applications or jump server
  ingress {
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    security_groups = [aws_security_group.apps.id, aws_security_group.jump.id]
  }

  # Allow all inbound postgres traffic from runner security group
  # TODO: Temp. Should be loading data from "build" Terraform files
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    security_groups = ["sg-0ee4f2c5012bee8c8"]
  }

  # Allow all inbound SQL traffic from me
  # TODO: Remove me!
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["212.36.33.254/32"]
  }

  # Allow all outbound traffic.
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "db" {
  backup_retention_period = 0
  allocated_storage       = 20
  db_subnet_group_name    = aws_db_subnet_group.db.name
  engine                  = "postgres"
  engine_version          = "11.4"
  identifier              = "insights"
  instance_class          = "db.t2.micro"
  multi_az                = false
  name                    = "insights"
  parameter_group_name    = aws_db_parameter_group.db.name
  password                = var.db_password
  username                = "root"
  port                    = 5432
  publicly_accessible     = false
  storage_encrypted       = false
  apply_immediately       = true
  storage_type            = "gp2"
  vpc_security_group_ids  = [aws_security_group.db.id]
  deletion_protection     = true
  iam_database_authentication_enabled = false
}

resource "aws_db_parameter_group" "db" {
  name   = "rds-pg"
  family = "postgres11"

  parameter {
    name  = "client_encoding"
    value = "UTF8"
  }
}

resource "aws_db_subnet_group" "db" {
  name       = "main"
  subnet_ids = [aws_subnet.sub1.id, aws_subnet.sub2.id]
}

resource "random_uuid" "value" {}

resource "postgresql_role" "user" {
  name = "user"
  password = random_uuid.value.result # Generate random password
  login = true
}

resource "postgresql_schema" "schema" {
  name  = "hello"
  owner = aws_db_instance.db.username

  policy {
    usage = true
    create = true
    role  = postgresql_role.user.name
  }
}

resource "aws_secretsmanager_secret" "svc" {
  name = "db-credentials"
}

resource "aws_secretsmanager_secret_version" "db-creds" {
  secret_id     = aws_secretsmanager_secret.svc.id
  secret_string = jsonencode({
    username = postgresql_role.user.name
    password = postgresql_role.user.password
  })
}
